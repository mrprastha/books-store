<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'/admin'],function(){
    Route::get('/login','AdminLoginController@login');
    Route::post('/login','AdminLoginController@verify')->name('admins.verify');
    Route::get('/register','AdminLoginController@register')->name('admins.register');
    Route::post('/register','AdminLoginController@store')->name('admins.store');
    Route::get('/logout',function(){
        session()->forget('USER_EMAIL');
        return redirect('/admin/login');
    });

});


Route::group(['middleware'=>['admin']],function(){
    Route::resource('/admin/books','BookController');
});




