<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\DB;

class AdminLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('admin.index');
    }


    public function register()
    {
        return view('admin.register');
    }

    public function verify(Request $request)
    {
        if($request->isMethod('post')){
            // $data = $request->all();
            $email = $request->email;
            $password = $request->password;
            $result = DB::table('tbl_admins')->where('email',$email)
                                             ->where('password',$password)->get();

// echo "<pre>";
// print_r($result);
// exit;

                                             if(isset($result['0']->id)){
                                                 $request->session()->put('USER_EMAIL',$result['0']->email);
                                                  return redirect('admin/books');
                                             }else{
                                         $request->session()->flash('msg',"Please Enter Valid Login Details");
                                         return redirect('admin/login');
                                             }
            exit;
        }
    }


    public function store(Request $request)
    {
        $data = new Admin();
        $data->email = $request->email;
        $data->password = $request->password;
        $data->save();
        return redirect('/admin/login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
