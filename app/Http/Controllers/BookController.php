<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{

    public function index()
    {
       return view('books.index')->with('books',Book::all());
    }


    public function create()
    {
        return view('books.create');
    }


    public function store(Request $request)
    {
        $book = new Book();
        $book->title = $request->title;
        $book->author_name = $request->author_name;
        $book->published_date = $request->published_date;
        // $book->title = $request->title;
        // $book->title = $request->title;
        $book->save();
        $request->session()->flash('msg','Data inserted Successfully');
        return redirect('/admin/books');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $book = Book::find($id);
        return view('books.edit')->with('book',$book);
    }


    public function update(Request $request, $id)
    {
        $book = Book::find($id);
        $book->title = $request->title;
        $book->author_name = $request->author_name;
        $book->published_date = $request->published_date;
        // $book->title = $request->title;
        // $book->title = $request->title;
        $book->save();
        $request->session()->flash('msg','Data Updated Successfully');
        return redirect('/admin/books');
    }

    public function destroy(Request $request, $id)
    {
        $book = new Book();
        $book->destroy($id);
        $request->session()->flash('msg','Data Deleted Successfully');
        return redirect('/admin/books');

    }
}
