<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

<script src="https://code.jquery.com/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
</head>
<body>
     {{-- @include('layouts.master') --}}
{{-- @section('content') --}}
<div class="container">
    <!-- jquery validation -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Register</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form action="{{route('admins.store')}}" method="post">@csrf
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
          {{-- <div class="form-group mb-0">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
              <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
            </div>
          </div> --}}
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Register</button>
         <button class="btn btn-danger">Cancel</button>
        </div>
      </form>
    </div>
    <!-- /.card -->
    </div>
{{-- @endsection --}}

</body>
</html>
