@extends('layouts.master')
@section('content')
@if(Session::has('msg'))
<div class="alert alert-success text-center style="align-center" >
    {{session('msg')}}
</div>
@endif

<div class="container" style="margin-top:30px">
            <div class="col-auto float-right ml-auto">
                <a href="{{route('books.create')}}" class='btn btn-primary'>Add Books</a>
            </div>
            <div class="container">
                <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Available Books</h3>

                    <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                        <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                            <i class="fas fa-search"></i>
                        </button>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- /.card-header -->
{{-- {{session()->all();}} --}}


                {{-- {{session()->all();}} --}}

                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Author Name</th>
                        <th>Published_date</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($books as $book)
                        <tr>
                        <td>{{$loop->index+1}}</td>
                        <td>{{$book->title}}</td>
                        <td>{{$book->author_name}}</td>
                        <td>{{$book->published_date}}</td>
                        <td>
                            <form action="{{route('books.destroy',$book->id)}}" method="post">
                                @csrf @method('delete')
                            <a href="{{route('books.edit',$book->id)}}" class="btn btn-warning btn-xs"><i class="fa fa-pencil-alt"></i></a>
                            <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>


                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
@endsection
