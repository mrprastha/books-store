@extends('layouts.master');

@section('content')
<div class="pull-right">
    <a href="{{url('/admin/books')}}" class="btn btn-success">View Books</a>
</div>
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Quick Example</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="{{route('books.update', $book->id)}}">
            @csrf @method('put')
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" value="{{$book->title}}" name="title" id="title" placeholder="Enter Title">
                </div>
                <div class="form-group">
                    <label for="author_name">Author Name</label>
                    <input type="text" class="form-control" value="{{$book->author_name}}" name="author_name" id="author_name"
                        placeholder="Enter Author Title">
                </div>
                <div class="form-group">
                    <label for="published_date">File input</label>
                    <input type="date" name="published_date" value="{{$book->published_date}}" id="published_date" class="form-control">
                </div>



              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="#" class="btn btn-danger">Cancel</a>
            </div>
        </form>
    </div>
<!-- /.card -->

<!-- general form elements -->
</div>
@endsection

